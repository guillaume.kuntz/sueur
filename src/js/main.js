var game = new SweatyPromoClient.offline()

var explosion = '<img src="./src/img/explosion.gif" class="explosion"/>'
var etoile = '<img src="./src/img/Étoile.png" class="etoile"/>'
var empty = '<img src="./src/img/empty.png" class="etoile"/>'
var vaiseau = '<img src="./src/img/vaiseau.png" class="vaiseau"/>'
var displayZone = document.getElementById('displayZone')
var player = false;

game.on('matrix', (matrix) => {
    // updateDisplay(matrix)
    var rowsElements = document.getElementsByClassName("row");
    for(var i = 0; i < matrix.length; i++){
        var cellsElements = rowsElements[i].children;
        for(var j = 0; j < matrix[i].length; j++){
            if(matrix[i][j] === 1) {
                cellsElements[j].innerHTML = etoile
             }else if(matrix[i][j] === 0) {
                cellsElements[j].innerHTML = empty
            }if (i === 7 && j == game.position ) {
                if (player == true) {
                    cellsElements[j].innerHTML = vaiseau
                }else{
                    cellsElements[j].innerHTML = explosion
                }
            }
        }
    }
    if (matrix[6][game.getPosition()] === 1 && matrix[6][game.getPosition() - 1]=== 0) {
        game.left() ;
    }
    else if (matrix[6][game.getPosition()] === 1 && matrix[6][game.getPosition() + 1]=== 0){
        game.right() ;
    }
    else if (game.getPosition() < 2 && matrix[6][game.getPosition()+1] === 0){
        game.right();
    }
    else if (game.getPosition() > 2 && matrix[6][game.getPosition()-1] === 0){
        game.left();
    }
})

// reception de l'évenement game over
game.on('sweaty', () => {
    document.getElementById('gameOverMessage').style.visibility = 'visible'
    document.getElementById('startButton').style.visibility = 'visible'
    console.log('En sueur !')
    player = false;
})

game.on('message', (messageData) => {
    console.log('message', messageData)
    displayMessage(messageData.author, messageData.message)
})

// affiche un message
function displayMessage(author, message, isMe = false) {
    var displayMessage = document.getElementById('messageZone')

    var element = document.createElement("div")
    var subEl = null
    if (isMe) {
        subEl = document.createElement("b")
    } else {
        subEl = document.createElement("span")
    }

    subEl.innerText = author

    element.appendChild(subEl)
    element.appendChild(document.createTextNode(' : ' + message))

    displayMessage.prepend(element)

}

// envoi un message (version en ligne)
function sendMessage() {
    var message = document.getElementById('messageInput').value
    if (game.sendMessage(message)) {
        // affiche pour moi
        displayMessage('moi', message, true)
        // reset l'input
        document.getElementById('messageInput').value = null
    }

}

function startButton() {
    document.getElementById('gameOverMessage').style.visibility = 'hidden'
    document.getElementById('startButton').style.visibility = 'hidden'
    player = true;
    game.start()
}

function loginButton() {
    game.login()
}

function droite() {
    game.right()
}

function gauche() {
    game.left()
}

function collision(){
    game.playerInGame;
}
 game.on('sweaty', function() {
 })
